<?php

declare(strict_types=1);

namespace Game\SharedKernel\Testing\Fakes;

use Game\Ports\EventDispatcher\EventDispatcherInterface;

class EventDispatcher implements EventDispatcherInterface
{
    private $dispatched;

    public function __construct()
    {
        $this->dispatched = [];
    }

    public function dispatch($event)
    {
        $this->dispatched[] = get_class($event);
    }

    public function seeEvent(string $eventName): bool
    {
        return in_array($eventName, $this->dispatched);
    }
}