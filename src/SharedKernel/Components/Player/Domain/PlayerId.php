<?php

declare(strict_types=1);

namespace Game\SharedKernel\Components\Player\Domain;

class PlayerId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}