<?php

declare(strict_types=1);

namespace Game\SharedKernel\Components\Planet\Domain;

use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\SharedKernel\Ports\AbstractEvent;

class ResourcesWereCollected extends AbstractEvent
{
    private $planetId;

    public function __construct(PlanetId $planetId)
    {
        $this->planetId = $planetId;
    }
}