<?php

declare(strict_types=1);

namespace Game\SharedKernel\Ports;

use Symfony\Contracts\EventDispatcher\Event;

abstract class AbstractEvent extends Event implements EventInterface
{

}