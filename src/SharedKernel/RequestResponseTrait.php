<?php

declare(strict_types=1);

namespace Game\SharedKernel;

use Game\SharedKernel\Exceptions\GameRuntimeException;

trait RequestResponseTrait
{
    public function __call($key, $arguments)
    {
        return $this->execute($key);
    }

    public function __get($key)
    {
        return $this->execute($key);
    }

    private function execute($key)
    {
        $this->validate($key);
        return $this->{$key};
    }

    private function validate($key): void
    {
        if (is_null($this->{$key})) {
            throw new GameRuntimeException('Property cannot be nullable');
        }
    }
}