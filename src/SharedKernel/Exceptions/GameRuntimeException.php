<?php

declare(strict_types=1);

namespace Game\SharedKernel\Exceptions;

class GameRuntimeException extends \RuntimeException
{

}