<?php

declare(strict_types=1);

namespace Game\Ports\EventDispatcher;

interface EventDispatcherInterface extends \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
{

}
