<?php

declare(strict_types=1);

namespace Game\Ports\Auth;

use Game\Components\Player\Domain\Email;
use Game\Components\Player\Domain\Password;
use Game\SharedKernel\Components\Player\Domain\PlayerId;

interface AuthenticationService
{
    /**
     * @return PlayerId
     * @throws NoUserAuthenticatedException
     */
    public function playerId(): PlayerId;

    /**
     * @return bool
     */
    public function isGuest(): bool;

    /**
     * @param PlayerId $playerId
     * @return mixed
     */
    public function loginById(PlayerId $playerId);

    /**
     * @return void
     */
    public function logout(): void;

    /**
     * @param Email $email
     * @param string $unprotectedPassword
     * @return string
     * @throws InvalidCredentialsException
     */
    public function attempt(Email $email, string $unprotectedPassword): string;
}