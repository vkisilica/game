<?php

declare(strict_types=1);

namespace Game\Ports\Auth;

use Game\SharedKernel\Exceptions\GameRuntimeException;

class InvalidCredentialsException extends GameRuntimeException
{

}