<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\UseCases\GetPlanet;

use Game\Components\Planet\Application\UseCases\ResponseInterface;
use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Response
 * @package Game\Components\Planet\Application\UseCases\GetPlanet
 * @method name
 * @method level
 * @method metal
 * @method polymer
 * @method metalToUpgrade
 * @method polymerToUpgrade
 */
final class Response implements ResponseInterface
{
    use RequestResponseTrait;

    /**
     * @var string
     */
    private $name;
    /**
     * @var int
     */
    private $level;
    /**
     * @var int
     */
    private $metal;
    /**
     * @var int
     */
    private $polymer;
    /**
     * @var int
     */
    private $metalToUpgrade;
    /**
     * @var int
     */
    private $polymerToUpgrade;

    /**
     * Create default response
     *
     * @param string $name
     * @param int $level
     * @param int $metal
     * @param int $polymer
     * @param int $metalToUpgrade
     * @param int $polymerToUpgrade
     * @return Response
     */
    public static function createDefaultResponse(string $name, int $level, int $metal, int $polymer, int $metalToUpgrade, int $polymerToUpgrade)
    {
        $instance = new static();
        $instance->name = $name;
        $instance->level = $level;
        $instance->metal = $metal;
        $instance->polymer = $polymer;
        $instance->metalToUpgrade = $metalToUpgrade;
        $instance->polymerToUpgrade = $polymerToUpgrade;
        return $instance;
    }
}