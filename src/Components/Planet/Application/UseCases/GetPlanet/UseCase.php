<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\UseCases\GetPlanet;

use Game\Components\Planet\Application\Persistence\PlanetQueryRepository;
use Game\Components\Planet\Application\Persistence\PlanetSpecificationFactory;
use Game\Components\Planet\Application\UseCases\ResponseInterface;
use Game\Components\Planet\Application\UseCases\UseCaseInterface;
use Game\Ports\Auth\AuthenticationService;
use Game\Components\Planet\Application\UseCases\GameOver\UseCase as GameOverUseCase;

final class UseCase implements UseCaseInterface
{
    private $authenticationService;
    private $repository;
    private $factory;

    /**
     * UseCase constructor.
     * @param AuthenticationService $authenticationService
     * @param PlanetQueryRepository $repository
     * @param PlanetSpecificationFactory $factory
     */
    private function __construct(AuthenticationService $authenticationService, PlanetQueryRepository $repository, PlanetSpecificationFactory $factory)
    {
        $this->authenticationService = $authenticationService;
        $this->repository = $repository;
        $this->factory = $factory;
    }

    /**
     * @param AuthenticationService $authenticationService
     * @param PlanetQueryRepository $repository
     * @param PlanetSpecificationFactory $factory
     * @return GameOverUseCase
     */
    public static function create(AuthenticationService $authenticationService, PlanetQueryRepository $repository, PlanetSpecificationFactory $factory): GameOverUseCase
    {
        $instance = new static($authenticationService, $repository, $factory);
        return new GameOverUseCase($instance, $repository);
    }

    /**
     * @return Response
     */
    public function handle(): ResponseInterface
    {
        $userId = $this->authenticationService->playerId();
        $planet = $this->repository->findOne($this->factory->getPlanetSpecification($userId));

        return Response::createDefaultResponse(
            $planet->name()->getValue(),
            $planet->level()->getValue(),
            $planet->metals()->getValue(),
            $planet->polymers()->getValue(),
            $planet->needMetalsToUpgrade()->getValue(),
            $planet->needPolymersToUpgrade()->getValue(),
        );
    }
}