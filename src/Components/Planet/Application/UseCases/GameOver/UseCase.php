<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\UseCases\GameOver;

use Game\Components\Planet\Application\Persistence\PlanetQueryRepository;
use Game\Components\Planet\Application\UseCases\ResponseInterface;
use Game\Components\Planet\Application\UseCases\UseCaseInterface;
use Game\Components\Planet\Domain\GameOverException;

final class UseCase implements UseCaseInterface
{
    /** @var PlanetQueryRepository */
    private $repository;

    private $useCase;

    /**
     * @param UseCaseInterface $useCase
     * @param PlanetQueryRepository $repository
     */
    public function __construct(UseCaseInterface $useCase, PlanetQueryRepository $repository)
    {
        $this->useCase = $useCase;
        $this->repository = $repository;
    }

    /**
     * @return ResponseInterface
     */
    public function handle(): ResponseInterface
    {
        try {
            $response = $this->useCase->handle();
        } catch (GameOverException $e) {
            $planet = $e->getPlanet();
            $this->repository->save($planet);
            $response = Response::createGameOverResponse();
        }
        return $response;
    }
}