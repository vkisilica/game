<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\UseCases\GameOver;

use Game\Components\Planet\Application\UseCases\ResponseInterface;
use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Response
 * @package Game\Components\Planet\Application\UseCases\GameOver
 * @method isGameOver
 */
final class Response implements ResponseInterface
{
    use RequestResponseTrait;

    private $isGameOver;

    /**
     * Response constructor.
     */
    private function __construct()
    {
        $this->isGameOver = false;
    }

    /**
     * Create game over response
     *
     * @return Response
     */
    public static function createGameOverResponse()
    {
        return (new static())->setGameOver();
    }

    public function setGameOver()
    {
        $this->isGameOver = true;
        return $this;
    }
}