<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\UseCases;

final class EmptyResponse implements ResponseInterface {}