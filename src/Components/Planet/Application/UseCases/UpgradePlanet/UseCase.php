<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\UseCases\UpgradePlanet;

use Game\Components\Planet\Application\Persistence\PlanetQueryRepository;
use Game\Components\Planet\Application\Persistence\PlanetSpecificationFactory;
use Game\Components\Planet\Application\UseCases\EmptyResponse;
use Game\Components\Planet\Application\UseCases\ResponseInterface;
use Game\Components\Planet\Application\UseCases\UseCaseInterface;
use Game\Components\Planet\Domain\NotEnoughResourcesException;
use Game\Ports\Auth\AuthenticationService;
use Game\Components\Planet\Application\UseCases\GameOver\UseCase as GameOverUseCase;

final class UseCase implements UseCaseInterface
{
    private $authenticationService;
    private $repository;
    private $factory;

    private function __construct(AuthenticationService $authenticationService, PlanetQueryRepository $repository, PlanetSpecificationFactory $factory)
    {
        $this->authenticationService = $authenticationService;
        $this->repository = $repository;
        $this->factory = $factory;
    }

    public static function create(AuthenticationService $authenticationService, PlanetQueryRepository $repository, PlanetSpecificationFactory $factory): \Game\Components\Planet\Application\UseCases\GameOver\UseCase
    {
        $instance = new static($authenticationService, $repository, $factory);
        return new GameOverUseCase($instance, $repository);
    }

    /**
     * Get a planet information
     *
     * @throws NotEnoughResourcesException
     */
    public function handle(): ResponseInterface
    {
        $userId = $this->authenticationService->playerId();
        $planet = $this->repository->findOne($this->factory->getPlanetSpecification($userId));

        $planet->upgrade();

        $this->repository->save($planet);

        return new EmptyResponse();
    }
}