<?php

namespace Game\Components\Planet\Application\Persistence;

use Game\Components\Planet\Domain\Planet;
use Game\SharedKernel\Exceptions\GameRuntimeException;

class PlanetNotSavedException extends GameRuntimeException
{
    private $planet;

    public static function create(Planet $planet)
    {
        $instance = new static();
        $instance->planet = $planet;

        return $instance;
    }

    /**
     * @return Planet
     */
    public function getPlanet(): Planet
    {
        return $this->planet;
    }
}