<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\Persistence;

use Game\Components\Planet\Domain\Planet;

interface PlanetQueryRepository
{
    public function findOne(PlanetSpecification $specification): Planet;
    public function save(Planet $planet);
}