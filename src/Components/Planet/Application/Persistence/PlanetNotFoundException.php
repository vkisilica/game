<?php

namespace Game\Components\Planet\Application\Persistence;

use Game\SharedKernel\Exceptions\GameRuntimeException;

class PlanetNotFoundException extends GameRuntimeException
{
    private $specification;

    public static function create(PlanetSpecification $specification)
    {
        $instance = new static();
        $instance->specification = $specification;

        return $instance;
    }

    /**
     * @return PlanetSpecification
     */
    public function getSpecification(): PlanetSpecification
    {
        return $this->specification;
    }
}