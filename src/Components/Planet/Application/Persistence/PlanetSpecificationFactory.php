<?php

declare(strict_types=1);

namespace Game\Components\Planet\Application\Persistence;

use Game\SharedKernel\Components\Player\Domain\PlayerId;

interface PlanetSpecificationFactory
{
    public function getPlanetSpecification(PlayerId $planetId): PlanetSpecification;
}