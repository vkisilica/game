<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\ValueObjects;

/** @todo add symfony validation. Rules: greater than 0, max 255 */
class Level
{
    /** @var int */
    private $value;

    /**
     * Level constructor.
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param Level $level
     * @return $this
     */
    public function increase(self $level): self
    {
        return new static($this->getValue() + $level->getValue());
    }

    /**
     * @param Level $level
     * @return bool
     */
    public function equalTo(self $level): bool
    {
        return $this->getValue() === $level->getValue();
    }
}