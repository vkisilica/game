<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\ValueObjects;

/** @todo add symfony validation. Rules: string, min length 1, max length 32 */
class Name
{
    /** @var string */
    private $value;

    /**
     * Name constructor.
     * @param string $value
     */
    public function __construct(string $value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue(): string
    {
        return $this->value;
    }
}