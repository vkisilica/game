<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\ValueObjects;

use Carbon\Carbon;
use Game\SharedKernel\Exceptions\InvalidArgumentException;

abstract class AbstractDateTime
{
    private $value;

    /**
     * AbstractDateTime constructor.
     * @param Carbon $value
     */
    public function __construct(Carbon $value)
    {
        $this->value = $value;
    }

    /**
     * @return Carbon
     */
    public function getValue(): Carbon
    {
        return $this->value;
    }

    /**
     * @param AbstractDateTime $resource
     * @return bool
     */
    public function equalTo(AbstractDateTime $resource): bool
    {
        $this->validate($resource);
        return $this->getValue()->equalTo($resource->getValue());
    }

    /**
     * Throws the InvalidArgumentException if the current and the input objects are instances from different classes
     *
     * @param AbstractDateTime $resource
     */
    private function validate(AbstractDateTime $resource)
    {
        if (! $resource instanceof static) {
            throw new InvalidArgumentException('Different object operation is not allowed');
        }
    }
}