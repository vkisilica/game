<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\ValueObjects;

use Carbon\Carbon;
use Game\SharedKernel\Exceptions\InvalidArgumentException;

class CountedAt extends AbstractDateTime
{
    /**
     * CountedAt constructor.
     * @param Carbon $value
     * @throws InvalidArgumentException
     */
    public function __construct(Carbon $value)
    {
        $this->validate($value);

        parent::__construct($value);
    }

    /**
     * Throws the InvalidArgumentException if the input Carbon instance contains future time stamp
     *
     * @param Carbon $value
     * @throws InvalidArgumentException
     */
    private function validate(Carbon $value)
    {
        if ($value->greaterThan(Carbon::now())) {
            throw new InvalidArgumentException('Time when resources were counted cannot be greater than the current time');
        }
    }
}