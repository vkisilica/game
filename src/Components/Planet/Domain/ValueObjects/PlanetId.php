<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\ValueObjects;

/**
 * Class PlanetId
 * @package Game\Components\Planet\Domain\ValueObjects
 */
class PlanetId
{
    /** @var mixed */
    private $value;

    /**
     * PlanetId constructor.
     * @param $value
     */
    public function __construct($value)
    {
        $this->value = $value;
    }

    /**
     * @return mixed
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string)$this->getValue();
    }
}