<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain;

use Game\SharedKernel\Exceptions\GameRuntimeException;
use Throwable;

class GameOverException extends GameRuntimeException
{
    /** @var Planet */
    private $planet;

    private function __construct(Planet $planet, $message = "", $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);

        $this->planet = $planet;
    }

    public static function fromPlanet(Planet $planet): self
    {
        return new static($planet, "Game over! You have collected enough resources", 0, null);
    }

    public function getPlanet(): Planet
    {
        return $this->planet;
    }
}