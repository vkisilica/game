<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain;

use Carbon\Carbon;
use Game\Components\Planet\Domain\Resources\Countable;
use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\Components\Planet\Domain\ValueObjects\ResourcesCollectedAt;
use Game\Components\Planet\Domain\ValueObjects\Level;
use Game\Components\Planet\Domain\ValueObjects\Name;
use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\SharedKernel\Components\Planet\Domain\ResourcesWereCollected;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\SharedKernel\Exceptions\GameRuntimeException;

final class Planet
{
    const NEED_METALS_FOR_GAME_OVER = 10000;
    const NEED_POLYMERS_FOR_GAME_OVER = 5000;

    private $id;
    private $playerId;
    private $name;
    private $level;
    private $metals;
    private $polymers;
    private $resourcesCountedAt;
    private $resourcesCollectedAt;

    // todo: Constructor should be short
    private function __construct(PlanetId $id, PlayerId $playerId, Name $name, Level $level, Metal $metals, Polymer $polymers, CountedAt $resourceCountedAt, ?ResourcesCollectedAt $resourceCollectedAt = null)
    {
        $this->id = $id;
        $this->playerId = $playerId;
        $this->name = $name;
        $this->level = $level;
        $this->metals = $metals;
        $this->polymers = $polymers;
        $this->resourcesCountedAt = $resourceCountedAt;
        $this->resourcesCollectedAt = $resourceCollectedAt;
    }

    /**
     * @param PlanetId $id
     * @param PlayerId $playerId
     * @param Name $name
     * @param Level $level
     * @param Metal $metals
     * @param Polymer $polymers
     * @param CountedAt $resourceCountedAt
     * @return static
     */
    public static function create(PlanetId $id, PlayerId $playerId, Name $name, Level $level, Metal $metals, Polymer $polymers, CountedAt $resourceCountedAt): self
    {
        $instance = static::hydrate(...func_get_args());
        $instance->hasEnoughResourcesForGameOver();
        return $instance;
    }

    /**
     * @param PlanetId $id
     * @param PlayerId $playerId
     * @param Name $name
     * @param Level $level
     * @param Metal $metals
     * @param Polymer $polymers
     * @param CountedAt $resourceCountedAt
     * @param ResourcesCollectedAt $resourceCollectedAt
     * @return static
     */
    public static function hydrate(PlanetId $id, PlayerId $playerId, Name $name, Level $level, Metal $metals, Polymer $polymers, CountedAt $resourceCountedAt, ?ResourcesCollectedAt $resourceCollectedAt = null): self
    {
        return new static(...func_get_args());
    }

    /**
     * @return void
     * @throws NotEnoughResourcesException
     */
    public function upgrade(): void
    {
        if ($this->isCollectedResources()) {
            throw new GameRuntimeException('Upgrade is not available because the planet has collected resources');
        }

        $needMetalsToUpgrade = $this->needMetalsToUpgrade();
        $needPolymersToUpgrade = $this->needPolymersToUpgrade();

        $metalsAreEnough = $this->metals()->equalToOrGreaterThan($needMetalsToUpgrade);
        $polymersAreEnough = $this->polymers()->equalToOrGreaterThan($needPolymersToUpgrade);

        if ($metalsAreEnough && $polymersAreEnough) {
            $this->metals = $this->metals()->decrease($needMetalsToUpgrade);
            $this->polymers = $this->polymers()->decrease($needPolymersToUpgrade);
            $this->resourcesCountedAt = new CountedAt(Carbon::now());

            $oneLevel = new Level(1);
            $this->level = $this->level()->increase($oneLevel);
            return;
        }
        throw new NotEnoughResourcesException('Planet has not enough resource to upgrade level');
    }

    /**
     * Check if a planet has enough resource for game over
     */
    private function hasEnoughResourcesForGameOver()
    {
        $metalsAreEnough = $this->metals()->equalToOrGreaterThan(new Metal(self::NEED_METALS_FOR_GAME_OVER));
        $polymersAreEnough = $this->polymers()->equalToOrGreaterThan(new Polymer(self::NEED_POLYMERS_FOR_GAME_OVER));

        if ($metalsAreEnough && $polymersAreEnough) {
            $this->markAsResourceCollected();
            /** todo: Wrong logic. The event should be dispatched after the planet object saving */
            DomainRegistry::instance()->getEventDispatcher()->dispatch(new ResourcesWereCollected($this->id()));
            throw GameOverException::fromPlanet($this);
        }
    }

    /**
     * Calculate and return current value of metals
     *
     * @return Countable
     */
    public function metals(): Countable
    {
        $metals = (int)floor($this->metalsAsFloat());

        return new Metal($metals);
    }

    /**
     * Calculate and return current value of polymers
     *
     * @return Countable
     */
    public function polymers(): Countable
    {
        $polymers = (int)floor($this->polymersAsFloat());

        return new Polymer($polymers);
    }

    /**
     * @return Metal
     */
    public function minesMetalsPerHour(): Metal
    {
        $metalCount = (int)(floor($this->minesMetalsPerHourAsFloat()));
        return new Metal($metalCount);
    }

    /**
     * @return float
     */
    private function metalsAsFloat(): float
    {
        $secondsHavePassed = $this->resourcesCountedAt->getValue()->diffInSeconds(Carbon::now());
        $increaseMetalsOn = (int)floor($this->minesMetalsPerHourAsFloat() * ($secondsHavePassed / 3600));

        return $this->metals->getValue() + $increaseMetalsOn;
    }

    /**
     * @return float
     */
    private function minesMetalsPerHourAsFloat(): float
    {
        return $this->minesMetalsPerSecondAsFloat() * 3600;
    }

    /**
     * @return float
     */
    private function minesMetalsPerSecondAsFloat(): float
    {
        return 20 * 1.5**($this->level()->getValue() - 1) / 3600;
    }

    /**
     * @return Polymer
     */
    public function minesPolymersPerHour(): Polymer
    {
        $polymerCount = (int)(floor($this->minesPolymersPerHourAsFloat()));
        return new Polymer($polymerCount);
    }

    /**
     * @return float
     */
    private function polymersAsFloat(): float
    {
        $secondsHavePassed = $this->resourcesCountedAt->getValue()->diffInSeconds(Carbon::now());
        $increasePolymersOn = (int)floor($this->minesPolymersPerHourAsFloat() * ($secondsHavePassed / 3600));

        return $this->polymers->getValue() + $increasePolymersOn;
    }

    /**
     * @return float
     */
    private function minesPolymersPerHourAsFloat(): float
    {
        return $this->minesPolymersPerSecondAsFloat() * 3600;
    }

    /**
     * @return float
     */
    private function minesPolymersPerSecondAsFloat(): float
    {
        return 10 * 1.5**($this->level()->getValue() - 1) / 3600;
    }

    /**
     * @return Metal
     */
    public function needMetalsToUpgrade(): Metal
    {
        $metalCount = (int)(floor(4 * 2**($this->level()->getValue() - 1)));
        return new Metal($metalCount);
    }

    /**
     * @return Polymer
     */
    public function needPolymersToUpgrade(): Polymer
    {
        $metalCount = (int)(floor(2 * 2**($this->level()->getValue() - 1)));
        return new Polymer($metalCount);
    }

    /**
     * Calculate when resources were collected
     */
    private function markAsResourceCollected(): void
    {
        $secondsHavePassed = $this->secondsPassedAfterGoalReached();
        $collectedAt = Carbon::now()->addSeconds($secondsHavePassed * -1);
        $resourceCollectedAt = new ResourcesCollectedAt($collectedAt);

        $this->resourcesCollectedAt = $resourceCollectedAt;
    }

    /**
     * @return float|int
     */
    private function secondsPassedAfterGoalReached()
    {
        $metalsCollectedSecondsAgo = $this->collectedMetalsSecondsAgo();
        $polymersCollectedSecondsAgo = $this->collectedPolymersSecondsAgo();

        if (($metalsCollectedSecondsAgo <=> $polymersCollectedSecondsAgo) <= 0) {
            $secondsHavePassed = $metalsCollectedSecondsAgo;
        } else {
            $secondsHavePassed = $polymersCollectedSecondsAgo;
        }

        return $secondsHavePassed;
    }

    /**
     * @return float|int
     */
    private function collectedMetalsSecondsAgo()
    {
        $metals = $this->metalsAsFloat();
        $metalsPerSecond = $this->minesMetalsPerSecondAsFloat();

        $metalsDiff = $metals - self::NEED_METALS_FOR_GAME_OVER;
        return $metalsDiff / $metalsPerSecond;
    }

    /**
     * @return float|int
     */
    private function collectedPolymersSecondsAgo()
    {
        $polymers = $this->polymersAsFloat();
        $polymersPerSecond = $this->minesPolymersPerSecondAsFloat();

        $polymersDiff = $polymers - self::NEED_POLYMERS_FOR_GAME_OVER;
        return $polymersDiff / $polymersPerSecond;
    }

    /**
     * @return bool
     */
    public function isCollectedResources(): bool
    {
        return !is_null($this->resourcesCollectedAt);
    }

    /**
     * @return PlayerId
     */
    public function playerId(): PlayerId
    {
        return $this->playerId;
    }

    /**
     * @return Name
     */
    public function name(): Name
    {
        return $this->name;
    }

    /**
     * @return Level
     */
    public function level(): Level
    {
        return $this->level;
    }

    /**
     * @return CountedAt
     */
    public function resourcesCountedAt(): CountedAt
    {
        return $this->resourcesCountedAt;
    }

    /**
     * @return null|ResourcesCollectedAt
     */
    public function resourcesCollectedAt(): ?ResourcesCollectedAt
    {
        return $this->resourcesCollectedAt;
    }

    /**
     * @return PlanetId
     */
    public function id(): PlanetId
    {
        return $this->id;
    }
}