<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\Resources;

interface Countable
{
    public function equalTo(self $valueObject): bool;
    public function equalToOrGreaterThan(self $resource): bool;
    public function increase(self $resource): self;
    public function decrease(self $resource): self;
}