<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\Resources;

/**
 * @todo: Think about interface for value object. Should they have an interface? Maybe, realization of a value object must not be able to be replaced. That way keep protected business logic from changes by interface.
 */
final class Metal extends AbstractCountable
{
}