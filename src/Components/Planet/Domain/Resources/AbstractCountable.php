<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain\Resources;

use Game\SharedKernel\Exceptions\InvalidArgumentException;

abstract class AbstractCountable implements Countable
{
    /** @var int */
    private $value;

    /**
     * AbstractResource constructor.
     * @param int $value
     */
    public function __construct(int $value)
    {
        $this->value = $value;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * Plus two objects and return result
     *
     * @param Countable $resource
     * @return Countable
     */
    public function increase(Countable $resource): Countable
    {
        $this->validate($resource);
        return new static($this->getValue() + $resource->getValue());
    }

    /**
     * Minus two objects and return result
     *
     * @param Countable $resource
     * @return Countable
     */
    public function decrease(Countable $resource): Countable
    {
        $this->validate($resource);
        return new static($this->getValue() - $resource->getValue());
    }

    /**
     * @param Countable $resource
     * @return bool
     */
    public function equalToOrGreaterThan(Countable $resource): bool
    {
        $this->validate($resource);
        return $this->equalTo($resource) || $this->getValue() > $resource->getValue();
    }

    /**
     * @param Countable $resource
     * @return bool
     */
    public function equalTo(Countable $resource): bool
    {
        $this->validate($resource);
        return $this->getValue() === $resource->getValue();
    }

    /**
     * @param Countable $resource
     * @throws InvalidArgumentException
     */
    private function validate(Countable $resource)
    {
        if (! $resource instanceof static) {
            throw new InvalidArgumentException('Different object operation is not allowed');
        }
    }
}