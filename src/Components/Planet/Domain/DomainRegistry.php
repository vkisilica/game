<?php

declare(strict_types=1);

namespace Game\Components\Planet\Domain;

use Game\Ports\EventDispatcher\EventDispatcherInterface;
use Game\SharedKernel\Exceptions\NotFoundDependencyException;

final class DomainRegistry
{
    /** @var self */
    private static $instance = null;

    /** @var EventDispatcherInterface */
    private $eventDispatcher = null;

    /**
     * DomainRegistry constructor.
     */
    private function __construct()
    {

    }

    /**
     * @return self
     */
    public static function instance(): self
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    /**
     * @throws \BadMethodCallException
     */
    public function __clone()
    {
        throw new \BadMethodCallException('Clone is not supported');
    }

    /**
     * @return EventDispatcherInterface
     * @throws NotFoundDependencyException
     */
    public function getEventDispatcher(): EventDispatcherInterface
    {
        if (is_null($this->eventDispatcher)) {
            throw new NotFoundDependencyException();
        }
        return $this->eventDispatcher;
    }

    /**
     * @param EventDispatcherInterface $dispatcher
     */
    public function setEventDispatcher(EventDispatcherInterface $dispatcher): void
    {
        $this->eventDispatcher = $dispatcher;
    }
}