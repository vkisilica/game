<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases;

use Game\SharedKernel\Exceptions\GameRuntimeException;

class SomePlayerHasAlreadyLoggedInException extends GameRuntimeException
{

}