<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\RegisterPlayer;

use Game\Components\Player\Domain\Email;
use Game\SharedKernel\Exceptions\GameRuntimeException;

class PlayerHasBeenAlreadyRegisteredException extends GameRuntimeException
{
    /**
     * @var Email
     */
    private $email;

    public static function create(Email $email)
    {
        $scalarValue = $email->getValue();
        $instance = new static("Player with \"$scalarValue\" email has already registered");
        $instance->email = $email;

        return $instance;
    }

    public function email(): Email
    {
        return $this->email;
    }
}