<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\RegisterPlayer;

use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Response
 * @package Game\Components\Player\Application\UseCases\RegisterPlayer
 * @method id
 * @method email
 */
final class Response
{
    use RequestResponseTrait;

    /** @var mixed */
    private $id;
    /** @var string */
    private $email;

    /**
     * Response constructor.
     * @param mixed $id
     * @param string $email
     */
    private function __construct($id, string $email)
    {
        $this->id = $id;
        $this->email = $email;
    }

    /**
     * Create default response
     *
     * @param mixed $id
     * @param string $email
     * @return Response
     */
    public static function createDefaultResponse($id, string $email)
    {
        return new static($id, $email);
    }
}