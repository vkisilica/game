<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\RegisterPlayer;

use Game\Components\Player\Application\CryptService;
use Game\Components\Player\Application\Persistence\PlayerSpecificationFactory;
use Game\Components\Player\Application\Persistence\PlayerRepository;
use Game\Components\Player\Application\UseCases\SomePlayerHasAlreadyLoggedInException;
use Game\Components\Player\Domain\Email;
use Game\Components\Player\Domain\Password;
use Game\Components\Player\Domain\Player;
use Game\Ports\Auth\AuthenticationService;

final class UseCase
{
    /** @var AuthenticationService  */
    private $authenticationService;
    /** @var PlayerRepository  */
    private $repository;
    /** @var PlayerSpecificationFactory */
    private $playerSpecificationFactory;
    /** @var CryptService */
    private $cryptService;

    /**
     * UseCase constructor.
     * @param AuthenticationService $authenticationService
     * @param PlayerRepository $repository
     * @param PlayerSpecificationFactory $playerSpecificationFactory
     * @param CryptService $cryptService
     */
    public function __construct(AuthenticationService $authenticationService, CryptService $cryptService, PlayerRepository $repository, PlayerSpecificationFactory $playerSpecificationFactory)
    {
        $this->authenticationService = $authenticationService;
        $this->cryptService = $cryptService;
        $this->playerSpecificationFactory = $playerSpecificationFactory;
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     * @return Response
     * @throws SomePlayerHasAlreadyLoggedInException
     * @throws PlayerHasBeenAlreadyRegisteredException
     */
    public function handle(Request $request): Response
    {
        $password = $this->cryptService->hash($request->password());

        $email = new Email($request->email());
        $password = new Password($password);

        $this->isGuest();
        $this->isNotExists($email);

        $player = Player::register($email, $password);
        /** @var Player $player */
        $player = $this->repository->save($player);

        return Response::createDefaultResponse(
            $player->id()->getValue(),
            $player->email()->getValue(),
        );
    }

    /**
     * @throws SomePlayerHasAlreadyLoggedInException
     */
    private function isGuest()
    {
        if (!$this->authenticationService->isGuest()) {
            throw new SomePlayerHasAlreadyLoggedInException();
        }
    }

    /**
     * @param Email $email
     * @throws PlayerHasBeenAlreadyRegisteredException
     */
    private function isNotExists(Email $email)
    {
        $player = $this->repository->findOne($this->playerSpecificationFactory->playerByEmailSpecification($email));
        if ($player !== null) {
            throw PlayerHasBeenAlreadyRegisteredException::create($email);
        }
    }
}