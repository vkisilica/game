<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\LoginPlayer;

use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Response
 * @package Game\Components\Player\Application\UseCases\LoginPlayer
 * @method token
 * @method failed
 */
final class Response
{
    use RequestResponseTrait;

    /** @var string|null */
    private $token;
    /**
     * @var bool|null
     */
    private $failed;

    /**
     * Response constructor.
     * @param string $token
     * @param bool|null $failed
     */
    private function __construct(?string $token, ?bool $failed = false)
    {
        $this->token = $token;
        $this->failed = $failed;
    }

    /**
     * Create default response
     *
     * @param string $token
     * @return Response
     */
    public static function createDefaultResponse(string $token)
    {
        return new static($token);
    }

    /**
     * Create error response
     *
     * @return Response
     */
    public static function createFailedResponse()
    {
        return new static(null, true);
    }
}