<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\LoginPlayer;

use Game\Components\Player\Domain\Email;
use Game\SharedKernel\Exceptions\GameRuntimeException;

class PlayerNotFoundException extends GameRuntimeException
{
    private $email;

    public static function create(Email $email): PlayerNotFoundException
    {
        $instance = new static('Player is not registered');
        $instance->setEmail($email);

        return $instance;
    }

    public function getEmail(): Email
    {
        return $this->email;
    }

    private function setEmail(Email $email): void
    {
        $this->email = $email;
    }
}