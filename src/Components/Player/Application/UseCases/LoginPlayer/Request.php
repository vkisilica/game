<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\LoginPlayer;

use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Request
 * @package Game\Components\Player\Application\UseCases\LoginPlayer
 * @method email
 * @method password
 */
final class Request
{
    use RequestResponseTrait;

    /**
     * @var string
     */
    private $email;

    /**
     * @var string
     */
    private $password;

    /**
     * Create default response
     *
     * @param string $email
     * @param string $password
     * @return self
     */
    public static function createDefaultRequest(string $email, string $password)
    {
        $instance = new static();
        $instance->email = $email;
        $instance->password = $password;
        return $instance;
    }
}