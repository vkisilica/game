<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\LogoutPlayer;

use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Response
 * @package Game\Components\Player\Application\UseCases\LogoutPlayer
 * @method message
 */
final class Response
{
    use RequestResponseTrait;

    /** @var string */
    private $message;

    /**
     * Response constructor.
     * @param string $message
     */
    private function __construct(string $message)
    {
        $this->message = $message;
    }

    /**
     * Create default response
     *
     * @param string $message
     * @return Response
     */
    public static function createDefaultResponse(string $message = '')
    {
        return new static($message);
    }
}