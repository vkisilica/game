<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\LogoutPlayer;

use Game\Components\Player\Application\CryptService;
use Game\Components\Player\Application\Persistence\PlayerSpecificationFactory;
use Game\Components\Player\Application\Persistence\PlayerRepository;
use Game\Components\Player\Application\UseCases\SomePlayerHasAlreadyLoggedInException;
use Game\Components\Player\Domain\Email;
use Game\Components\Player\Domain\Password;
use Game\Components\Player\Domain\Player;
use Game\Ports\Auth\AuthenticationService;
use Game\Ports\Auth\InvalidCredentialsException;

final class UseCase
{
    /** @var AuthenticationService  */
    private $authenticationService;

    /**
     * UseCase constructor.
     * @param AuthenticationService $authenticationService
     */
    public function __construct(AuthenticationService $authenticationService)
    {
        $this->authenticationService = $authenticationService;
    }

    /**
     * @param Request $request
     * @return Response
     */
    public function handle(Request $request)
    {
        $this->authenticationService->logout();

        return Response::createDefaultResponse('Logged out successfully.');
    }
}