<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\UseCases\LogoutPlayer;

use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Request
 * @package Game\Components\Player\Application\UseCases\LogoutPlayer
 */
final class Request
{
    use RequestResponseTrait;

    /**
     * Create default response
     *
     * @return self
     */
    public static function createDefaultRequest()
    {
        return new static();
    }
}