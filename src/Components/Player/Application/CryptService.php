<?php

declare(strict_types=1);

namespace Game\Components\Player\Application;

interface CryptService
{
    public function hash(string $input): string;
}