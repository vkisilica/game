<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\Persistence;

use Game\Components\Player\Domain\Email;
use Game\SharedKernel\Components\Player\Domain\PlayerId;

interface PlayerSpecificationFactory
{
    public function playerByEmailSpecification(Email $email): PlayerSpecification;
    public function playerByIdSpecification(PlayerId $playerId): PlayerSpecification;
}