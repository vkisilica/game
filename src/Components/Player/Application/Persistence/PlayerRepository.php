<?php

declare(strict_types=1);

namespace Game\Components\Player\Application\Persistence;

use Game\Components\Player\Domain\Player;

interface PlayerRepository
{
    public function findOne($specification): ?Player;
    public function save(Player $player): Player;
}