<?php

declare(strict_types=1);

namespace Game\Components\Player\Domain;

use Webmozart\Assert\Assert;

final class Password
{
    /** @var string */
    private $value;

    public function __construct(string $password)
    {
        $this->value = $password;
    }

    public function getValue(): string
    {
        return $this->value;
    }

    private function toString(): string
    {
        return $this->getValue();
    }

    public function __toString()
    {
        return $this->toString();
    }
}