<?php

declare(strict_types=1);

namespace Game\Components\Player\Domain;

use Game\SharedKernel\Components\Player\Domain\PlayerId;

final class Player
{
    /** @var PlayerId  */
    private $id;
    /** @var Email  */
    private $email;
    /** @var Password|null  */
    private $password;

    private function __construct(?PlayerId $id, Email $email, ?Password $password = null)
    {
        $this->id = $id;
        $this->email = $email;
        $this->password = $password;
    }

    public static function register(Email $email, Password $password)
    {
        return new self(null, $email, $password);
    }

    public static function restore(PlayerId $id, Email $email)
    {
        return new self($id, $email);
    }

    public function id(): ?PlayerId
    {
        return $this->id;
    }

    public function email(): Email
    {
        return $this->email;
    }

    public function password(): ?Password
    {
        return $this->password;
    }
}