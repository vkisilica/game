<?php

declare(strict_types=1);

namespace Game\Components\Rating\Domain;

use Game\SharedKernel\Exceptions\InvalidArgumentException;

/** @todo Should I add game duration into rating? Maybe, the better way will be if starting and finishing date will be stored in Planet entity. I should think about consistency. */
final class Second
{
    private $value;

    public function __construct(int $value)
    {
        $this->validate($value);
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }

    private function validate(int $value)
    {
        if ($value < 0) {
            throw new InvalidArgumentException('Value cannot be negative');
        }
    }
}