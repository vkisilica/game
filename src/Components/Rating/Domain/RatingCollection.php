<?php

namespace Game\Components\Rating\Domain;

use Countable;

/**
 * Class RatingCollection
 * @package Game\Components\Rating\Domain
 */
final class RatingCollection implements Countable
{
    /** @var array */
    private $ratings = [];

    /**
     * @param Rating $rating
     */
    public function push(Rating $rating): void
    {
        $this->ratings[$rating->getId()->getValue()] = $rating;
    }

    /**
     * @return int
     */
    public function count(): int
    {
        return count($this->ratings);
    }

    /**
     * @param Rating $rating
     * @return bool
     */
    public function contains(Rating $rating): bool
    {
        return array_key_exists($rating->getId()->getValue(), $this->ratings);
    }
}