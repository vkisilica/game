<?php

namespace Game\Components\Rating\Domain;

final class Rank
{
    private $value;

    public function __construct(int $value)
    {
        $this->value = $value;
    }

    public function getValue(): int
    {
        return $this->value;
    }
}