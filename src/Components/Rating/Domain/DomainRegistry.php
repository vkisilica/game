<?php

declare(strict_types=1);

namespace Game\Components\Rating\Domain;

use Game\Ports\EventDispatcher\EventDispatcherInterface;
use Game\SharedKernel\Exceptions\NotFoundDependencyException;

final class DomainRegistry
{
    /** @var self */
    private static $instance = null;

    /** @var EventDispatcherInterface */
    private $eventDispatcher = null;

    private function __construct()
    {

    }

    public static function instance()
    {
        if (is_null(self::$instance)) {
            self::$instance = new self();
        }
        return self::$instance;
    }

    public function __clone()
    {
        throw new \BadMethodCallException('Clone is not supported');
    }

    public function getEventDispatcher(): EventDispatcherInterface
    {
        if (is_null($this->eventDispatcher)) {
            throw new NotFoundDependencyException();
        }
        return $this->eventDispatcher;
    }

    public function setEventDispatcher(EventDispatcherInterface $dispatcher): void
    {
        $this->eventDispatcher = $dispatcher;
    }
}