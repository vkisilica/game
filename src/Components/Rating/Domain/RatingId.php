<?php

namespace Game\Components\Rating\Domain;

final class RatingId
{
    private $value;

    public function __construct($value)
    {
        $this->value = $value;
    }

    public function getValue()
    {
        return $this->value;
    }
}