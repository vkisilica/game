<?php


namespace Game\Components\Rating\Domain;

final class Rating
{
    /**
     * @var RatingId
     */
    private $id;
    /**
     * @var Rank
     */
    private $rank;
    /**
     * @var Second
     */
    private $gameDuration;

    private function __construct(?RatingId $id, ?Rank $rank, Second $gameDuration)
    {
        $this->id = $id;
        $this->rank = $rank;
        $this->gameDuration = $gameDuration;
    }

    public static function create(Second $gameDuration, RatingId $id = null, Rank $rank = null)
    {
        $instance = new static($id, $rank, $gameDuration);
        return $instance;
    }

    public function getId(): RatingId
    {
        return $this->id;
    }
}