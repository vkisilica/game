<?php

declare(strict_types=1);

namespace Game\Components\Rating\Application\UseCases\CreateRating;

use Game\Components\Rating\Domain\RatingCollection;
use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Response
 * @method rating
 */
final class Response
{
    use RequestResponseTrait;

    /**
     * @var RatingCollection
     */
    private $rating;

    /**
     * Create default response
     *
     * @param RatingCollection $rating
     * @return self
     */
    public static function create(RatingCollection $rating)
    {
        $instance = new static();
        $instance->rating = $rating;
        return $instance;
    }
}