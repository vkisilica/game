<?php

declare(strict_types=1);

namespace Game\Components\Rating\Application\UseCases\CreateRating;

use Game\SharedKernel\RequestResponseTrait;

/**
 * Class Request
 *
 * @method timeInterval
 */
final class Request
{
    use RequestResponseTrait;

    /**
     * Game duration in seconds
     *
     * @var int
     */
    private $gameDuration;

    /**
     * Create request
     *
     * @param int $timeInterval
     * @return self
     */
    public static function create(int $timeInterval)
    {
        $instance = new static();
        $instance->gameDuration = $timeInterval;
        return $instance;
    }
}