<?php

declare(strict_types=1);

namespace Game\Components\Rating\Application\UseCases\CreateRating;

use Game\Components\Rating\Application\Persistence\RatingQueryRepository;
use Game\Components\Rating\Domain\Rating;
use Game\Components\Rating\Domain\Second;

final class UseCase
{
    /**
     * @var RatingQueryRepository
     */
    private $repository;

    public function __construct(RatingQueryRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param Request $request
     *
     * @return void
     * @throws \Exception
     */
    public function handle(Request $request): void
    {
        $playedTime = new Second($request->gameDuration());
        $rating = Rating::create($playedTime);

        $this->repository->save($rating);
    }
}