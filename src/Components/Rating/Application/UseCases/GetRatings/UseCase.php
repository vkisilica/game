<?php

declare(strict_types=1);

namespace Game\Components\Rating\Application\UseCases\GetRatings;

use Game\Components\Rating\Application\Persistence\RatingQueryRepository;
use Game\Components\Rating\Application\Persistence\RatingSpecificationFactory;

final class UseCase
{
    /**
     * @var RatingSpecificationFactory
     */
    private $factory;

    /**
     * @var RatingQueryRepository
     */
    private $repository;

    public function __construct(RatingSpecificationFactory $factory, RatingQueryRepository $repository)
    {
        $this->factory = $factory;
        $this->repository = $repository;
    }

    /**
     * @return Response
     */
    public function handle(): Response
    {
        $rating = $this->repository->query($this->factory->getRatingSpecification());

        return Response::create(
            $rating
        );
    }
}