<?php


namespace Game\Components\Rating\Application\Persistence;

use Game\Components\Rating\Domain\Rating;
use Game\Components\Rating\Domain\RatingCollection;

interface RatingQueryRepository
{
    public function query(RatingSpecification $specification): RatingCollection;
    public function save(Rating $rating);
}