<?php

declare(strict_types=1);

namespace Game\Components\Rating\Application\Persistence;

interface RatingSpecificationFactory
{
    public function getRatingSpecification(): RatingSpecification;
}