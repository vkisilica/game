<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain;

use Carbon\Carbon;
use Game\Components\Planet\Domain\GameOverException;
use Game\Components\Planet\Domain\Planet;
use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\Components\Planet\Domain\ValueObjects\ResourcesCollectedAt;
use Game\Components\Planet\Domain\ValueObjects\Level;
use Game\Components\Planet\Domain\ValueObjects\Name;
use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\Tests\Components\Planet\PlanetTestCase;

class GameOverExceptionTest extends PlanetTestCase
{
    /** @test */
    public function getPlanet()
    {
        $planetId = new PlanetId(1);
        $playerId = new PlayerId(2);
        $name = new Name('test');
        $level = new Level(2);
        $metals = new Metal(1000);
        $polymers = new Polymer(500);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());
        $planet = Planet::hydrate($planetId, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $exception = GameOverException::fromPlanet($planet);

        $this->assertSame($planet, $exception->getPlanet());
        $this->assertSame('Game over! You have collected enough resources', $exception->getMessage());
    }
}