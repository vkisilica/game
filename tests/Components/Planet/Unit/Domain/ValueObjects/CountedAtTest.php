<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain\ValueObjects;

use Carbon\Carbon;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\SharedKernel\Exceptions\InvalidArgumentException;

class CountedAtTest extends DateTimeTestCase
{
    const DATETIME_CLASS = CountedAt::class;

    /** @test */
    public function should_throw_invalid_argument_exception_when_future_date_is_received()
    {
        $class = $this->dateTimeClassName();

        $inHour = Carbon::now()->addHour();

        try {
            new $class($inHour);
        } catch (InvalidArgumentException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail("Exception was not thrown");
    }
}