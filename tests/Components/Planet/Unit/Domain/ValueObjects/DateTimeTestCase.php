<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain\ValueObjects;

use Carbon\Carbon;
use Game\Tests\Components\Planet\PlanetTestCase;

abstract class DateTimeTestCase extends PlanetTestCase
{
    protected function dateTimeClassName()
    {
        return static::DATETIME_CLASS;
    }

    /** @test */
    public function should_return_carbon_date()
    {
        $class = $this->dateTimeClassName();

        $inHour = Carbon::now()->addHour(-1);
        $countedAt = new $class($inHour);
        $this->assertEquals($inHour, $countedAt->getValue());
    }

    /** @test */
    public function should_be_able_to_compare_with_the_equal_objects()
    {
        $class = $this->dateTimeClassName();

        Carbon::setTestNow(Carbon::now());

        $inHour = Carbon::now()->addHour(-1);
        $countedAt = new $class($inHour);

        $this->assertFalse($countedAt->equalTo(new $class(Carbon::now())));

        $this->assertTrue($countedAt->equalTo(new $class(Carbon::now()->addHour(-1))));
    }
}