<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain\ValueObjects;

use Game\Components\Planet\Domain\ValueObjects\ResourcesCollectedAt;

class CollectedAtTest extends DateTimeTestCase
{
    const DATETIME_CLASS = ResourcesCollectedAt::class;
}