<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain;

use Carbon\Carbon;
use Game\Components\Planet\Domain\GameOverException;
use Game\Components\Planet\Domain\NotEnoughResourcesException;
use Game\Components\Planet\Domain\Planet;
use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\Components\Planet\Domain\ValueObjects\ResourcesCollectedAt;
use Game\Components\Planet\Domain\ValueObjects\Level;
use Game\Components\Planet\Domain\ValueObjects\Name;
use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\SharedKernel\Components\Planet\Domain\ResourcesWereCollected;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\SharedKernel\Testing\Fakes\EventDispatcher;
use Game\Tests\Components\Planet\PlanetTestCase;

class PlanetTest extends PlanetTestCase
{
    public function resourceProductionProgressProvider()
    {
        return array(
            array(1, 20, 10),
            array(2, 30, 15),
            array(3, 45, 22),
            array(4, 67, 33),
            array(5, 101, 50),
            array(6, 151, 75),
            array(7, 227, 113),
            array(8, 341, 170),
            array(9, 512, 256),
            array(10, 768, 384),
        );
    }

    /**
     * @test
     * @param int $level
     * @param int $metalAmount
     * @param int $polymerAmount
     * @dataProvider resourceProductionProgressProvider
     */
    public function should_increase_resource_production_with_increasing_levels_in_geometric_progression($level, $metalAmount, $polymerAmount)
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level($level);
        $metals = new Metal(0);
        $polymers = new Polymer(0);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertEquals($metalAmount, $planet->minesMetalsPerHour()->getValue());
        $this->assertEquals($polymerAmount, $planet->minesPolymersPerHour()->getValue());
    }

    public function levelCostProgressProvider()
    {
        return array(
            array(1, 4, 2),
            array(2, 8, 4),
            array(3, 16, 8),
            array(4, 32, 16),
            array(5, 64, 32),
            array(6, 128, 64),
            array(7, 256, 128),
            array(8, 512, 256),
            array(9, 1024, 512),
            array(10, 2048, 1024),
        );
    }

    /**
     * @test
     * @param int $level
     * @param int $metalAmount
     * @param int $polymerAmount
     * @dataProvider levelCostProgressProvider
     */
    public function should_increase_level_cost_in_geometric_progression_with_increasing_levels($level, $metalAmount, $polymerAmount)
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level($level);
        $metals = new Metal(0);
        $polymers = new Polymer(0);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertEquals($metalAmount, $planet->needMetalsToUpgrade()->getValue());
        $this->assertEquals($polymerAmount, $planet->needPolymersToUpgrade()->getValue());
    }

    /**
     * @test
     * @throws NotEnoughResourcesException
     */
    public function planet_should_has_upgrading_feature()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(20);
        $polymers = new Polymer(10);
        $countedAt = new CountedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        $planet->upgrade();

        $expectedLevel = new Level(2);
        $this->assertTrue($planet->level()->equalTo($expectedLevel));
    }

    /**
     * @test
     * @expectedException \Game\Components\Planet\Domain\NotEnoughResourcesException
     */
    public function planet_should_throw_not_enough_resources_exception_if_metals_are_not_enough_to_upgrading()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(0);
        $polymers = new Polymer(1000);
        $countedAt = new CountedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        $planet->upgrade();
    }

    /**
     * @test
     * @expectedException \Game\Components\Planet\Domain\NotEnoughResourcesException
     */
    public function planet_should_throw_not_enough_resources_exception_if_polymers_are_not_enough_to_upgrading()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(1000);
        $polymers = new Polymer(0);
        $countedAt = new CountedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        $planet->upgrade();
    }

    /** @test */
    public function should_calculate_metals_that_it_has()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(1000);
        $polymers = new Polymer(0);

        $countedAt = new CountedAt(Carbon::now());
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        $this->assertEquals(1000, $planet->metals()->getValue());

        $countedAt = new CountedAt(Carbon::now()->addHour(-1));
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        $this->assertEquals(1020, $planet->metals()->getValue());

        $metals = new Metal(10);
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        $this->assertEquals(30, $planet->metals()->getValue());
    }

    /** @test */
    public function should_calculate_polymers_that_it_has()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(0);
        $polymers = new Polymer(500);
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $countedAt = new CountedAt(Carbon::now());
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(500, $planet->polymers()->getValue());

        $countedAt = new CountedAt(Carbon::now()->addHour(-1));
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(510, $planet->polymers()->getValue());

        $polymers = new Polymer(5);
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(15, $planet->polymers()->getValue());
    }

    /** @test */
    public function should_calculate_metals_mining_per_hour()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(0);
        $polymers = new Polymer(0);
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $countedAt = new CountedAt(Carbon::now());
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(20, $planet->minesMetalsPerHour()->getValue());

        $level = new Level(2);
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(30, $planet->minesMetalsPerHour()->getValue());

        $level = new Level(10);
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(768, $planet->minesMetalsPerHour()->getValue());
    }

    /** @test */
    public function should_calculate_polymers_making_per_hour()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(0);
        $polymers = new Polymer(0);
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $countedAt = new CountedAt(Carbon::now());
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(10, $planet->minesPolymersPerHour()->getValue());

        $level = new Level(2);
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(15, $planet->minesPolymersPerHour()->getValue());

        $level = new Level(10);
        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $this->assertEquals(384, $planet->minesPolymersPerHour()->getValue());
    }

    /** @test */
    public function should_throw_game_over_exception_1()
    {
        try {
            $id = new PlanetId('testID');
            $playerId = new PlayerId(2);
            $name = new Name('');
            $level = new Level(1);
            $metals = new Metal(10000);
            $polymers = new Polymer(5000);
            $countedAt = new CountedAt(Carbon::now());

            Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        } catch (GameOverException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('GameOverException was not thrown');
    }

    /** @test */
    public function should_throw_game_over_exception_2()
    {
        try {
            $id = new PlanetId('testID');
            $playerId = new PlayerId(2);
            $name = new Name('');
            $level = new Level(1);
            $metals = new Metal(10001);
            $polymers = new Polymer(5001);
            $countedAt = new CountedAt(Carbon::now());

            Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        } catch (GameOverException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('GameOverException was not thrown');
    }

    /** @test */
    public function should_not_throw_game_over_exception_1()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(9999);
        $polymers = new Polymer(3500);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertInstanceOf(Planet::class, $planet);
    }

    /** @test */
    public function should_not_throw_game_over_exception_2()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(10000);
        $polymers = new Polymer(3499);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $planet = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertInstanceOf(Planet::class, $planet);
    }

    /** @test */
    public function should_raise_game_over_event()
    {
        try {
            $id = new PlanetId('testID');
            $playerId = new PlayerId(2);
            $name = new Name('');
            $level = new Level(1);
            $metals = new Metal(10001);
            $polymers = new Polymer(5001);
            $countedAt = new CountedAt(Carbon::now());

            Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        } catch (GameOverException $e) {
            $this->assertDispatched(ResourcesWereCollected::class);
            return;
        }

        $this->fail('GameOverException was not thrown');
    }

    /** @test */
    public function should_create_a_planet_instance_without_game_over_exception_raising()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(10001);
        $polymers = new Polymer(3501);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $planet = Planet::hydrate($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertInstanceOf(Planet::class, $planet);
    }

    /** @test */
    public function should_be_able_to_set_resource_collected_property()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(10001);
        $polymers = new Polymer(3501);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());

        $planet = Planet::hydrate($id, $playerId, $name, $level, $metals, $polymers, $countedAt);

        $this->assertFalse($planet->isCollectedResources());

        $planet = Planet::hydrate($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertTrue($planet->isCollectedResources());
    }

    /** @test */
    public function planet_should_be_marked_as_collected_resource_with_calculation_when_it_was_happen()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-m-d H:i:s', '2020-01-11 12:00:00'));

        $id = new PlanetId('testID');
        $playerId = new PlayerId(5);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(9998);
        $polymers = new Polymer(4999);
        $countedAt = new CountedAt(Carbon::now()->addHours(-1));

        try {
            Planet::create($id, $playerId, $name, $level, $metals, $polymers, $countedAt);
        } catch (GameOverException $e) {
            $planet = $e->getPlanet();

            $this->assertTrue($planet->isCollectedResources());

            $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now()->addMinutes(-54));
            $this->assertTrue($planet->resourcesCollectedAt()->equalTo($resourceCollectedAt));
        }
    }

    /** @test */
    public function planet_should_return_resource_collected_at_value()
    {
        $id = new PlanetId('testID');
        $playerId = new PlayerId(5);
        $name = new Name('');
        $level = new Level(1);
        $metals = new Metal(9998);
        $polymers = new Polymer(4999);
        $countedAt = new CountedAt(Carbon::now()->addHours(-1));
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now()->addMinutes(-54));

        $planet = Planet::hydrate($id, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);

        $this->assertTrue($planet->resourcesCollectedAt()->equalTo($resourceCollectedAt));
    }

    private function getFakeEventDispatcher(): EventDispatcher
    {
        return $this->eventDispatcher;
    }


    protected function assertDispatched(string $eventName)
    {
        $this->assertTrue($this->getFakeEventDispatcher()->seeEvent($eventName));
    }
}