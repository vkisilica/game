<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain\Resources;

use Game\Components\Planet\Domain\Resources\Polymer;

class PolymerTest extends ResourceTestCase
{
    const RESOURCE = Polymer::class;
}