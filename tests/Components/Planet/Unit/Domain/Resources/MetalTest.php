<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain\Resources;

use Game\Components\Planet\Domain\Resources\Metal;

class MetalTest extends ResourceTestCase
{
    const RESOURCE = Metal::class;
}