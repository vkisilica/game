<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\Unit\Domain\Resources;

use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\SharedKernel\Exceptions\InvalidArgumentException;
use Game\Tests\Components\Planet\PlanetTestCase;

abstract class ResourceTestCase extends PlanetTestCase
{
    protected function resourceClassName()
    {
        return static::RESOURCE;
    }

    /** @test */
    public function should_return_int()
    {
        $class = $this->resourceClassName();
        /** @var Metal $resource */
        $resource = new $class(1000);
        $this->assertEquals(1000, $resource->getValue());
    }

    /** @test */
    public function should_be_able_add_metals()
    {
        $class = $this->resourceClassName();
        /** @var Metal $resource1 */
        $resource1 = new $class(100);
        /** @var Metal $resource2 */
        $resource2 = new $class(50);

        /** @var Metal $resource3 */
        $resource3 = $resource1->increase($resource2);

        $this->assertEquals(100, $resource1->getValue());
        $this->assertEquals(50, $resource2->getValue());
        $this->assertEquals(150, $resource3->getValue());
    }

    /** @test */
    public function should_be_able_minus_metals()
    {
        $class = $this->resourceClassName();
        /** @var Metal $resource1 */
        $resource1 = new $class(100);
        /** @var Metal $resource2 */
        $resource2 = new $class(50);

        /** @var Metal $resource3 */
        $resource3 = $resource1->decrease($resource2);

        $this->assertEquals(100, $resource1->getValue());
        $this->assertEquals(50, $resource2->getValue());
        $this->assertEquals(50, $resource3->getValue());
    }

    /** @test */
    public function should_be_able_to_compare()
    {
        $class = $this->resourceClassName();
        /** @var Metal $resource1 */
        $resource1 = new $class(100);
        /** @var Metal $resource2 */
        $resource2 = new $class(50);

        $this->assertTrue($resource1->equalToOrGreaterThan($resource2));
        $this->assertFalse($resource2->equalToOrGreaterThan($resource1));
    }

    /** @test */
    public function should_be_able_to_compare_2()
    {
        $class = $this->resourceClassName();
        /** @var Metal $resource1 */
        $resource1 = new $class(100);

        $anotherClass = $class === Metal::class ? Polymer::class : Metal::class;
        /** @var Metal $resource2 */
        $resource2 = new $anotherClass(50);

        try {
            $resource1->equalToOrGreaterThan($resource2);
        } catch (InvalidArgumentException $exception) {
            $this->assertSame('Different object operation is not allowed', $exception->getMessage());
            return;
        }

        $this->fail('That test should not pass');
    }
}