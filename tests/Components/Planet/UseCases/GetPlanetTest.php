<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\UseCases;

use Carbon\Carbon;
use Game\Components\Planet\Application\Persistence\PlanetQueryRepository;
use Game\Components\Planet\Application\Persistence\PlanetSpecification;
use Game\Components\Planet\Application\Persistence\PlanetSpecificationFactory;
use Game\Components\Planet\Application\UseCases\GetPlanet\Response;
use Game\Components\Planet\Application\UseCases\GetPlanet\UseCase;
use Game\Components\Planet\Domain\Planet;
use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\Components\Planet\Domain\ValueObjects\ResourcesCollectedAt;
use Game\Components\Planet\Domain\ValueObjects\Level;
use Game\Components\Planet\Domain\ValueObjects\Name;
use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\Ports\Auth\AuthenticationService;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\Tests\Components\Planet\PlanetTestCase;

class GetPlanetTest extends PlanetTestCase
{
    /** @test */
    public function should_return_response_with_resources()
    {
        $response = $this->useCase()->handle();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals('Vulcan', $response->name());
        $this->assertEquals(1, $response->level());
        $this->assertEquals(20, $response->metal());
        $this->assertEquals(10, $response->polymer());
        $this->assertEquals(4, $response->metalToUpgrade());
        $this->assertEquals(2, $response->polymerToUpgrade());
    }

    /**
     * it test that metals increase on 20 units and polymers increase on 10 units each hour
     *
     * @test
     */
    public function should_count_resource_since_last_counting()
    {
        $payload['resourceCountedAt'] = new CountedAt(Carbon::now()->addHour(-1));
        $response = $this->useCase($payload)->handle();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals('Vulcan', $response->name());
        $this->assertEquals(1, $response->level());
        $this->assertEquals(40, $response->metal());
        $this->assertEquals(20, $response->polymer());
    }

    /** @test */
    public function should_throw_resource_collected_exception()
    {
        Carbon::setTestNow(Carbon::createFromFormat('Y-d-m H:i:s', '2020-01-01 00:00:00'));
        $payload['resourceCountedAt'] = new CountedAt(Carbon::now()->addHours(-499));
        $payload['planetShouldBeSaved'] = true;

        $response = $this->useCase($payload)->handle();
        $this->assertInstanceOf(\Game\Components\Planet\Application\UseCases\GameOver\Response::class, $response);
        $this->assertTrue($response->isGameOver());
    }

    private function authenticationService()
    {
        $mock = $this->getMockBuilder(AuthenticationService::class)->setMethods(['playerId', 'isGuest', 'loginById', 'attempt', 'logout'])->getMock();
        $mock->method('playerId')->willReturn(new PlayerId(2));

        return $mock;
    }

    private function planetQueryRepository($payload)
    {
        Carbon::setTestNow(Carbon::now());

        if (!array_key_exists('resourceCountedAt', $payload) || is_null($payload['resourceCountedAt'])) {
            $payload['resourceCountedAt'] = new CountedAt(Carbon::now());
        }
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('Vulcan');
        $level = new Level(1);
        $metals = new Metal(20);
        $polymers = new Polymer(10);

        $mock = $this->getMockBuilder(PlanetQueryRepository::class)->setMethods(['findOne', 'save'])->getMock();
        $mock->method('findOne')->with($this->planetSpecification())->will($this->returnCallback(function() use ($id, $playerId, $name, $level, $metals, $polymers, $payload) {
            return Planet::create($id, $playerId, $name, $level, $metals, $polymers, $payload['resourceCountedAt']);
        }));

        if (array_key_exists('planetShouldBeSaved', $payload) && $payload['planetShouldBeSaved'] === true) {
            $planet = Planet::hydrate($id, $playerId, $name, $level, $metals, $polymers, $payload['resourceCountedAt'], new ResourcesCollectedAt(Carbon::now()));
            $mock->expects($this->once())->method('save')->with($planet)->willReturn(null);
        }

        return $mock;
    }

    private function planetSpecificationFactory()
    {
        $mock = $this->getMockBuilder(PlanetSpecificationFactory::class)->setMethods(['getPlanetSpecification'])->getMock();
        $mock->method('getPlanetSpecification')->with(new PlayerId(2))->willReturn($this->planetSpecification());

        return $mock;
    }

    private function planetSpecification()
    {
        return $this->getMockBuilder(PlanetSpecification::class)->getMock();
    }

    private function useCase($payload = [])
    {
        return UseCase::create($this->authenticationService(), $this->planetQueryRepository($payload), $this->planetSpecificationFactory());
    }
}