<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\UseCases;

use Carbon\Carbon;
use Game\Components\Planet\Application\Persistence\PlanetQueryRepository;
use Game\Components\Planet\Application\Persistence\PlanetSpecification;
use Game\Components\Planet\Application\Persistence\PlanetSpecificationFactory;
use Game\Components\Planet\Application\UseCases\EmptyResponse;
use Game\Components\Planet\Application\UseCases\UpgradePlanet\UseCase;
use Game\Components\Planet\Domain\NotEnoughResourcesException;
use Game\Components\Planet\Domain\Planet;
use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\Components\Planet\Domain\ValueObjects\Level;
use Game\Components\Planet\Domain\ValueObjects\Name;
use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\Ports\Auth\AuthenticationService;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\SharedKernel\Exceptions\GameRuntimeException;
use Game\Tests\Components\Planet\PlanetTestCase;

class UpgradePlanetTest extends PlanetTestCase
{
    public function setUp()
    {
        $knownDate = Carbon::create(2001, 5, 21, 12);
        Carbon::setTestNow($knownDate);
    }

    /** @test */
    public function should_be_able_upgrade_level()
    {
        $response = $this->useCase()->handle();

        $this->assertEquals(new EmptyResponse(), $response);
    }

    /**
     * @test
     */
    public function should_throw_not_enough_resources_exception()
    {
        try {
            $this->useCase(['metals' => 0, 'polymers' => 0, 'resourceCountedAt' => Carbon::now(), 'shouldCallSaveMethod' => false])->handle();
        } catch (NotEnoughResourcesException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('UseCase should throw NotEnoughResourcesException');
    }

    /** @test */
    public function should_throw_resource_collected_exception()
    {
        $payload['resourceCountedAt'] = Carbon::now()->addDays(-30);

        try {
            $this->useCase($payload)->handle();
        } catch (GameRuntimeException $exception) {
            $this->assertTrue(true);
            return;
        }
        $this->fail('Exception was not thrown');
    }


    private function planetQueryRepository(array $payload)
    {
        if (!array_key_exists('metals', $payload)) {
            $payload['metals'] = 20;
        }
        if (!array_key_exists('polymers', $payload)) {
            $payload['polymers'] = 10;
        }
        if (!array_key_exists('resourceCountedAt', $payload)) {
            $payload['resourceCountedAt'] = Carbon::now()->addHour(-1);
        }
        if (!array_key_exists('shouldCallSaveMethod', $payload)) {
            $payload['shouldCallSaveMethod'] = true;
        }
        $id = new PlanetId('testID');
        $playerId = new PlayerId(2);
        $name = new Name('Vulcan');
        $level = new Level(1);
        $metals = new Metal($payload['metals']);
        $polymers = new Polymer($payload['polymers']);
        $resourceCountedAt = new CountedAt($payload['resourceCountedAt']);

        $planetBeforeUpgrade = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $resourceCountedAt);

        $level = new Level(2);
        $metals = new Metal($payload['metals'] + $planetBeforeUpgrade->minesMetalsPerHour()->getValue() - 4);
        $polymers = new Polymer($payload['polymers'] + $planetBeforeUpgrade->minesPolymersPerHour()->getValue() - 2);
        $resourceCountedAt = new CountedAt(Carbon::now());
        $planetAfterUpgrade = Planet::create($id, $playerId, $name, $level, $metals, $polymers, $resourceCountedAt);

        $mock = $this->getMockBuilder(PlanetQueryRepository::class)->setMethods(['findOne', 'save'])->getMock();
        $mock->method('findOne')->with($this->planetSpecification())->willReturn($planetBeforeUpgrade);
        if ($payload['shouldCallSaveMethod']) {
            $mock->expects($this->exactly(1))->method('save')->with($planetAfterUpgrade);
        }

        return $mock;
    }

    private function authenticationService()
    {
        $mock = $this->getMockBuilder(AuthenticationService::class)->setMethods(['playerId', 'isGuest', 'loginById', 'attempt', 'logout'])->getMock();
        $mock->method('playerId')->willReturn(new PlayerId(2));

        return $mock;
    }

    private function planetSpecificationFactory()
    {
        $mock = $this->getMockBuilder(PlanetSpecificationFactory::class)->setMethods(['getPlanetSpecification'])->getMock();
        $mock->method('getPlanetSpecification')->with(new PlayerId(2))->willReturn($this->planetSpecification());

        return $mock;
    }

    private function planetSpecification()
    {
        return $this->getMockBuilder(PlanetSpecification::class)->getMock();
    }

    private function useCase($payload = [])
    {
        return UseCase::create($this->authenticationService(), $this->planetQueryRepository($payload), $this->planetSpecificationFactory());
    }
}