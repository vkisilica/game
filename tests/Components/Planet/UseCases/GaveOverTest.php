<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet\UseCases;

use Carbon\Carbon;
use Game\Components\Planet\Application\Persistence\PlanetQueryRepository;
use Game\Components\Planet\Application\UseCases\GameOver\Response;
use Game\Components\Planet\Application\UseCases\GameOver\UseCase;
use Game\Components\Planet\Application\UseCases\ResponseInterface;
use Game\Components\Planet\Application\UseCases\UseCaseInterface;
use Game\Components\Planet\Domain\GameOverException;
use Game\Components\Planet\Domain\Planet;
use Game\Components\Planet\Domain\Resources\Metal;
use Game\Components\Planet\Domain\Resources\Polymer;
use Game\Components\Planet\Domain\ValueObjects\CountedAt;
use Game\Components\Planet\Domain\ValueObjects\ResourcesCollectedAt;
use Game\Components\Planet\Domain\ValueObjects\Level;
use Game\Components\Planet\Domain\ValueObjects\Name;
use Game\Components\Planet\Domain\ValueObjects\PlanetId;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\Tests\Components\Planet\PlanetTestCase;

class GaveOverTest extends PlanetTestCase
{
    /** @test */
    public function game_over_exception_does_not_throw()
    {
        $mockResponse = $this->getMockBuilder(ResponseInterface::class)->getMock();

        $mockUseCase = $this->getMockBuilder(UseCaseInterface::class)->getMock();
        $mockUseCase->method('handle')->willReturn($mockResponse);

        $mockPlanetRepository = $this->getMockBuilder(PlanetQueryRepository::class)->getMock();

        $gameOverUseCase = new UseCase($mockUseCase, $mockPlanetRepository);

        $this->assertSame($mockResponse, $gameOverUseCase->handle());
    }

    /** @test */
    public function game_over_exception_throws()
    {
        $planetId = new PlanetId(1);
        $playerId = new PlayerId(2);
        $name = new Name('test');
        $level = new Level(2);
        $metals = new Metal(1000);
        $polymers = new Polymer(500);
        $countedAt = new CountedAt(Carbon::now());
        $resourceCollectedAt = new ResourcesCollectedAt(Carbon::now());
        $planet = Planet::hydrate($planetId, $playerId, $name, $level, $metals, $polymers, $countedAt, $resourceCollectedAt);
        $gameOverException = GameOverException::fromPlanet($planet);

        $mockUseCase = $this->getMockBuilder(UseCaseInterface::class)->getMock();
        $mockUseCase->method('handle')->willThrowException($gameOverException);

        $mockPlanetRepository = $this->getMockBuilder(PlanetQueryRepository::class)->getMock();
        $mockPlanetRepository->method('save')->with($planet)->willReturn(true);

        $gameOverUseCase = new UseCase($mockUseCase, $mockPlanetRepository);
        $this->assertEquals(Response::createGameOverResponse(), $gameOverUseCase->handle());
    }
}