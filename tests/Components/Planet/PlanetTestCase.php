<?php

declare(strict_types=1);

namespace Game\Tests\Components\Planet;

use Game\Components\Planet\Domain\DomainRegistry;
use Game\SharedKernel\Testing\Fakes\EventDispatcher;
use Game\Tests\ProjectTestCase;

class PlanetTestCase extends ProjectTestCase
{
    protected $eventDispatcher;

    public function setUp()
    {
        parent::setUp();

        $this->eventDispatcher = new EventDispatcher();

        DomainRegistry::instance()->setEventDispatcher($this->eventDispatcher);
    }
}