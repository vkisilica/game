<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\Unit\Domain;

use Game\Components\Player\Domain\Password;
use Game\Tests\Components\Player\PlayerTestCase;

class PasswordTest extends PlayerTestCase
{
    /** @test */
    public function valid_creating()
    {
        $string = 'some_password';
        $password = new Password($string);
        $this->assertInstanceOf(Password::class, $password);
        $this->assertEquals($string, $password->getValue());
    }
}