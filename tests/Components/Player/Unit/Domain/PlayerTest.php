<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\Unit\Domain;

use Game\Components\Player\Domain\Email;
use Game\Components\Player\Domain\Password;
use Game\Components\Player\Domain\Player;
use Game\Tests\Components\Player\PlayerTestCase;

class PlayerTest extends PlayerTestCase
{
    /** @test */
    public function can_be_registered()
    {
        $email = new Email('joe@test.com');
        $password = new Password('pass');
        $player = Player::register($email, $password);

        $this->assertInstanceOf(Player::class, $player);
//        $this->assertNull($player->id());
        $this->assertInstanceOf(Email::class, $player->email());
        $this->assertSame($email, $player->email());
        $this->assertInstanceOf(Password::class, $player->password());
        $this->assertSame($password, $player->password());
    }
}