<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\Unit\Domain;

use Game\Components\Player\Domain\Email;
use Game\Tests\Components\Player\PlayerTestCase;

class EmailTest extends PlayerTestCase
{
    /** @test */
    public function valid_creating()
    {
        $string = 'joe@test.com';
        $email = new Email($string);
        $this->assertInstanceOf(Email::class, $email);
        $this->assertEquals($string, $email->getValue());
    }

    /**
     * @test
     * @dataProvider validEmailDataProvider
     */
    public function get_value($string)
    {
        $email = new Email($string);
        $this->assertInstanceOf(Email::class, $email);
        $this->assertEquals($string, $email->getValue());
    }

    /**
     * @test
     * @dataProvider invalidEmailDataProvider
     * @param $email
     */
    public function invalid_creating_1($email)
    {
        $this->expectException(\InvalidArgumentException::class);
        new Email($email);
    }

    public function invalidEmailDataProvider()
    {
        return [
            ['joe'],
            ['joe.test.com'],
            ['joe@'],
            ['joe@test'],
            ['@test.com']
        ];
    }

    public function validEmailDataProvider()
    {
        return [
            ['joe.doe@example.com'],
            ['joe_doe@example.com'],
            ['Joe.Doe@sub.example.com'],
        ];
    }
}