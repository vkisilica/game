<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\Unit\Application;

use Game\Components\Player\Application\UseCases\RegisterPlayer\PlayerHasBeenAlreadyRegisteredException;
use Game\Components\Player\Domain\Email;
use Game\Tests\Components\Player\PlayerTestCase;

class PlayerHasBeenAlreadyRegisteredExceptionTest extends PlayerTestCase
{
    /** @test */
    public function basic()
    {
        $email = new Email('test@test.com');
        $exception = PlayerHasBeenAlreadyRegisteredException::create($email);
        $this->assertInstanceOf(PlayerHasBeenAlreadyRegisteredException::class, $exception);
        $this->assertInstanceOf(Email::class, $exception->email());
        $this->assertEquals('test@test.com', $exception->email());
    }
}