<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player;

use Game\Components\Planet\Domain\DomainRegistry;
use Game\SharedKernel\Testing\Fakes\EventDispatcher;
use Game\Tests\ProjectTestCase;

class PlayerTestCase extends ProjectTestCase
{
    protected $eventDispatcher;

    public function setUp()
    {
        parent::setUp();

        $this->eventDispatcher = new EventDispatcher();

        DomainRegistry::instance()->setEventDispatcher($this->eventDispatcher);
    }
}