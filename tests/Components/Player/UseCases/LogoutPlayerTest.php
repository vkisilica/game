<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\UseCases;

use Game\Components\Player\Application\UseCases\LogoutPlayer\Response;
use Game\Components\Player\Application\UseCases\LogoutPlayer\Request;
use Game\Components\Player\Application\UseCases\LogoutPlayer\UseCase;
use Game\Ports\Auth\AuthenticationService;
use Game\Tests\Components\Player\PlayerTestCase;

class LogoutPlayerTest extends PlayerTestCase
{
    /** @test */
    public function success_player_logout()
    {
        $response = $this->useCase()->handle(Request::createDefaultRequest());

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals('Logged out successfully.', $response->message());
    }

    private function authenticationService($payload)
    {
        $mock = $this->getMockBuilder(AuthenticationService::class)->setMethods(['isGuest', 'loginById', 'logout', 'playerId', 'attempt'])->getMock();
        $mock->expects($this->at(0))->method('logout');

        return $mock;
    }

    private function useCase($payload = [])
    {
        return new UseCase($this->authenticationService($payload));
    }
}