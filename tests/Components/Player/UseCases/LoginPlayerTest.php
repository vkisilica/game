<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\UseCases;

use Game\Components\Player\Application\CryptService;
use Game\Components\Player\Application\Persistence\PlayerSpecification;
use Game\Components\Player\Application\Persistence\PlayerSpecificationFactory;
use Game\Components\Player\Application\Persistence\PlayerRepository;
use Game\Components\Player\Application\UseCases\LoginPlayer\PlayerNotFoundException;
use Game\Components\Player\Application\UseCases\LoginPlayer\Response;
use Game\Components\Player\Application\UseCases\SomePlayerHasAlreadyLoggedInException;
use Game\Components\Player\Application\UseCases\LoginPlayer\UseCase;
use Game\Components\Player\Application\UseCases\LoginPlayer\Request;
use Game\Components\Player\Application\UseCases\LoginPlayer\PlayerHasBeenAlreadyRegisteredException;
use Game\Components\Player\Domain\Email;
use Game\Components\Player\Domain\Password;
use Game\Components\Player\Domain\Player;
use Game\Ports\Auth\AuthenticationService;
use Game\Ports\Auth\InvalidCredentialsException;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\Tests\Components\Player\PlayerTestCase;

class LoginPlayerTest extends PlayerTestCase
{
    /** @test */
    public function success_player_login()
    {
        $playerId = 'testId';
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);
        $response = $this->useCase([
            'data' => [
                'email' => $email
            ],
            'options' => [
                'playerIsExists' => true
            ]
        ])->handle($request);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertFalse($response->failed());
        $this->assertEquals('super_secret_token', $response->token());
    }

    /**
     * @test
     */
    public function invalid_password()
    {
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);

        $response = $this->useCase([
            'data' => [
                'email' => $email
            ],
            'options' => [
                'passwordMatch' => false
            ]
        ])->handle($request);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertTrue($response->failed());
    }

    /**
     * @test
     */
    public function player_not_found()
    {
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);
        try {
            $this->useCase([
                'data' => [
                    'email' => $email
                ],
                'options' => [
                    'playerIsExists' => false
                ]
            ])->handle($request);
        } catch (PlayerNotFoundException $e) {
            $this->assertEquals($email, $e->getEmail()->getValue());
            $this->assertTrue(true);
            return;
        }

        $this->fail('Exception was not thrown');
    }

    /**
     * @test
     */
    public function some_player_has_already_logged_in()
    {
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);
        try {
            $this->useCase([
                'data' => [
                    'email' => $email
                ],
                'options' => [
                    'requestIsFromGuest' => false
                ]
            ])->handle($request);
        } catch (SomePlayerHasAlreadyLoggedInException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('Exception was not thrown');
    }

    private function authenticationService($payload)
    {
        if (!isset($payload['options']['requestIsFromGuest'])) {
            $payload['options']['requestIsFromGuest'] = true;
        }
        if (!isset($payload['options']['passwordMatch'])) {
            $payload['options']['passwordMatch'] = true;
        }

        $mock = $this->getMockBuilder(AuthenticationService::class)->setMethods(['isGuest', 'loginById', 'playerId', 'attempt', 'logout'])->getMock();
        $mock->method('isGuest')->willReturn($payload['options']['requestIsFromGuest']);
        if ($payload['options']['passwordMatch']) {
            $mock->method('attempt')->with(new Email($payload['data']['email']), new Password('pass'))->willReturn('super_secret_token');
        } else {
            $mock->method('attempt')->willThrowException(new InvalidCredentialsException());
        }

        return $mock;
    }

    private function playerRepository($payload)
    {
        $mock = $this->getMockBuilder(PlayerRepository::class)->getMock();
        if (empty($payload)) {
            return $mock;
        }

        if (!isset($payload['options']['playerIsExists'])) {
            $payload['options']['playerIsExists'] = true;
        }

        $email = new Email($payload['data']['email']);
        $playerBeforeSaving = Player::register($email, new Password('ssap'));

        $playerId = new PlayerId('testId');
        $playerAfterSaving = Player::restore($playerId, $email);

        $mock->method('save')->with($playerBeforeSaving)->willReturn($playerAfterSaving);
        if ($payload['options']['playerIsExists'] === true) {
            $mock->method('findOne')->willReturn($playerAfterSaving);
        } else {
            $mock->method('findOne')->willReturn(null);
        }

        return $mock;
    }

    private function cryptService()
    {
        $mock = $this->getMockBuilder(CryptService::class)->getMock();
        $mock->method('hash')->with('pass')->willReturn('ssap');

        return $mock;
    }

    private function specificationFactory()
    {
        $mock = $this->getMockBuilder(PlayerSpecificationFactory::class)->setMethods(['playerByEmailSpecification', 'playerByIdSpecification'])->getMock();

        $specification = $this->getMockBuilder(PlayerSpecification::class)->getMock();
        $mock->method('playerByEmailSpecification')->willReturn($specification);

        return $mock;
    }

    private function useCase($payload = [])
    {
        return new UseCase($this->authenticationService($payload), $this->cryptService(), $this->playerRepository($payload), $this->specificationFactory());
    }
}