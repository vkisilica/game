<?php

declare(strict_types=1);

namespace Game\Tests\Components\Player\UseCases;

use Game\Components\Player\Application\CryptService;
use Game\Components\Player\Application\Persistence\PlayerSpecification;
use Game\Components\Player\Application\Persistence\PlayerSpecificationFactory;
use Game\Components\Player\Application\Persistence\PlayerRepository;
use Game\Components\Player\Application\UseCases\RegisterPlayer\Response;
use Game\Components\Player\Application\UseCases\SomePlayerHasAlreadyLoggedInException;
use Game\Components\Player\Application\UseCases\RegisterPlayer\UseCase;
use Game\Components\Player\Application\UseCases\RegisterPlayer\Request;
use Game\Components\Player\Domain\Email;
use Game\Components\Player\Domain\Password;
use Game\Components\Player\Domain\Player;
use Game\Components\Player\Application\UseCases\RegisterPlayer\PlayerHasBeenAlreadyRegisteredException;
use Game\Ports\Auth\AuthenticationService;
use Game\SharedKernel\Components\Player\Domain\PlayerId;
use Game\Tests\Components\Player\PlayerTestCase;

class RegisterPlayerTest extends PlayerTestCase
{
    /** @test */
    public function success_player_registration()
    {
        $playerId = 'testId';
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);
        $response = $this->useCase([
            'data' => [
                'email' => $email
            ],
            'options' => [
                'playerIsExists' => false
            ]
        ])->handle($request);

        $this->assertInstanceOf(Response::class, $response);
        $this->assertEquals($playerId, $response->id());
        $this->assertEquals($email, $response->email());
    }

    /** @test */
    public function player_has_already_registered()
    {
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);
        try {
            $this->useCase([
                'data' => [
                    'email' => $email
                ],
                'options' => [
                    'playerIsExists' => true
                ]
            ])->handle($request);
        } catch (PlayerHasBeenAlreadyRegisteredException $e) {
            $this->assertEquals("Player with \"$email\" email has already registered", $e->getMessage());
            $this->assertInstanceOf(Email::class, $e->email());
            $this->assertEquals($email, $e->email());
            return;
        }

        $this->fail('Exception was not thrown');
    }

    /**
     * @test
     * @todo
     */
    public function some_player_has_already_logged_in()
    {
        $email = 'john.doe@example.com';
        $password = 'pass';
        $request = Request::createDefaultRequest($email, $password);
        try {
            $this->useCase([
                'data' => [
                    'email' => $email
                ],
                'options' => [
                    'requestIsFromGuest' => false
                ]
            ])->handle($request);
        } catch (SomePlayerHasAlreadyLoggedInException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('Exception was not thrown');
    }

    private function authenticationService($payload)
    {
        if (!isset($payload['options']['requestIsFromGuest'])) {
            $payload['options']['requestIsFromGuest'] = true;
        }

        $mock = $this->getMockBuilder(AuthenticationService::class)->setMethods(['isGuest', 'loginById', 'playerId', 'attempt', 'logout'])->getMock();
        $mock->method('isGuest')->willReturn($payload['options']['requestIsFromGuest']);
        $mock->method('loginById')->with(new PlayerId('testId'))->willReturn(true);
        return $mock;
    }

    private function playerRepository($payload)
    {
        $mock = $this->getMockBuilder(PlayerRepository::class)->getMock();
        if (empty($payload)) {
            return $mock;
        }

        if (!isset($payload['options']['playerIsExists'])) {
            $payload['options']['playerIsExists'] = false;
        }

        $email = new Email($payload['data']['email']);
        $playerBeforeSaving = Player::register($email, new Password('ssap'));

        $playerId = new PlayerId('testId');
        $playerAfterSaving = Player::restore($playerId, $email);

        $mock->method('save')->with($playerBeforeSaving)->willReturn($playerAfterSaving);
        if ($payload['options']['playerIsExists'] === true) {
            $mock->method('findOne')->willReturn($playerAfterSaving);
        } else {
            $mock->method('findOne')->willReturn(null);
        }

        return $mock;
    }

    private function cryptService()
    {
        $mock = $this->getMockBuilder(CryptService::class)->getMock();
        $mock->method('hash')->with('pass')->willReturn('ssap');

        return $mock;
    }

    private function specificationFactory()
    {
        $mock = $this->getMockBuilder(PlayerSpecificationFactory::class)->setMethods(['playerByEmailSpecification', 'playerByIdSpecification'])->getMock();

        $specification = $this->getMockBuilder(PlayerSpecification::class)->getMock();
        $mock->method('playerByEmailSpecification')->willReturn($specification);

        return $mock;
    }

    private function useCase($payload = [])
    {
        return new UseCase($this->authenticationService($payload), $this->cryptService(), $this->playerRepository($payload), $this->specificationFactory());
    }
}