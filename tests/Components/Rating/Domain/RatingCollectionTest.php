<?php

declare(strict_types=1);

namespace Game\Tests\Components\Rating\Domain;

use DateInterval;
use Game\Components\Rating\Domain\Rank;
use Game\Components\Rating\Domain\Rating;
use Game\Components\Rating\Domain\RatingCollection;
use Game\Components\Rating\Domain\RatingId;
use Game\Components\Rating\Domain\Second;
use Game\Tests\Components\Rating\RatingTestCase;

class RatingCollectionTest extends RatingTestCase
{
    /** @test */
    public function should_push_rating_entity()
    {
        $ratingCollection = new RatingCollection();

        $ratingId = new RatingId('test');
        $rank = new Rank(1);
        $dateInterval = new Second(84600 * 2);
        $rating = Rating::create($dateInterval, $ratingId, $rank);

        $ratingCollection->push($rating);

        $this->assertObjectHasAttribute('ratings', $ratingCollection);
    }

    /** @test */
    public function should_not_push_rating_entity()
    {
        $ratingCollection = new RatingCollection();

        try {
            $ratingCollection->push('test');
        } catch (\TypeError $exception) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('Exception was not thrown');
    }

    /** @test */
    public function should_count_rating_entities()
    {
        $ratingCollection = new RatingCollection();

        $ratingId1 = new RatingId('test');
        $rank = new Rank(2);
        $dateInterval = new Second(84600 * 2);
        $rating1 = Rating::create($dateInterval, $ratingId1, $rank);

        $ratingId2 = new RatingId('test1');
        $rating2 = Rating::create($dateInterval, $ratingId2, $rank);

        $ratingCollection->push($rating1);

        $this->assertEquals(1, $ratingCollection->count());

        $ratingCollection->push($rating2);

        $this->assertEquals(2, $ratingCollection->count());

        $ratingCollection->push($rating1);

        $this->assertEquals(2, $ratingCollection->count());
    }

    /** @test */
    public function should_determine_contains_object_or_not()
    {
        $ratingCollection = new RatingCollection();

        $ratingId1 = new RatingId('test');
        $rank = new Rank(3);
        $dateInterval = new Second(84600 * 2);
        $rating1 = Rating::create($dateInterval, $ratingId1, $rank);

        $ratingId2 = new RatingId('test1');
        $rating2 = Rating::create($dateInterval, $ratingId2, $rank);

        $ratingCollection->push($rating1);
        $this->assertTrue($ratingCollection->contains($rating1));
        $this->assertFalse($ratingCollection->contains($rating2));

        $rating3 = Rating::create($dateInterval, $ratingId1, $rank);
        $this->assertTrue($ratingCollection->contains($rating3));
    }
}