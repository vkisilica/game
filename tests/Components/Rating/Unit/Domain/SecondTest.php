<?php

declare(strict_types=1);

namespace Game\Tests\Components\Rating\Unit\Domain;

use Game\Components\Rating\Domain\Second;
use Game\SharedKernel\Exceptions\InvalidArgumentException;
use Game\Tests\Components\Rating\RatingTestCase;

class SecondTest extends RatingTestCase
{
    /** @test */
    public function should_return_value()
    {
        $seconds = new Second(600);

        $this->assertSame(600, $seconds->getValue());
    }

    /** @test */
    public function should_return_only_unsigned_value()
    {
        try {
            new Second(-600);
        } catch (InvalidArgumentException $e) {
            $this->assertTrue(true);
            return;
        }

        $this->fail('Exception was not thrown');
    }
}