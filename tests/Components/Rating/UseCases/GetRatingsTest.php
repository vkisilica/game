<?php

declare(strict_types=1);

namespace Game\Tests\Components\Rating\UseCases;

use Game\Components\Rating\Application\Persistence\RatingQueryRepository;
use Game\Components\Rating\Application\Persistence\RatingSpecification;
use Game\Components\Rating\Application\Persistence\RatingSpecificationFactory;
use Game\Components\Rating\Application\UseCases\GetRatings\Response;
use Game\Components\Rating\Application\UseCases\GetRatings\UseCase;
use Game\Components\Rating\Domain\Rank;
use Game\Components\Rating\Domain\Rating;
use Game\Components\Rating\Domain\RatingCollection;
use Game\Components\Rating\Domain\RatingId;
use Game\Components\Rating\Domain\Second;
use Game\Tests\Components\Rating\RatingTestCase;

class GetRatingsTest extends RatingTestCase
{
    /**
     * @todo: add player id
     * @test
     */
    public function should_return_response_with_resources()
    {
        $response = $this->useCase()->handle();

        $this->assertInstanceOf(Response::class, $response);
        $this->assertInstanceOf(RatingCollection::class, $response->rating());
        $this->assertCount(3, $response->rating());

        $id = new RatingId('1');
        $rank = new Rank(1);
        $gameDuration = new Second(3779);
        $rating1 = Rating::create($gameDuration, $id, $rank);
        $this->assertTrue($response->rating()->contains($rating1));
    }

    private function useCase()
    {
        return new UseCase($this->ratingSpecificationFactory(), $this->ratingQueryRepository());
    }

    private function ratingQueryRepository()
    {
        $id = new RatingId('1');
        $rank = new Rank(1);
        $gameDuration = new Second(3779);
        $rating1 = Rating::create($gameDuration, $id, $rank);
        $id = new RatingId('2');
        $rank = new Rank(2);
        $gameDuration = new Second(3779);
        $rating2 = Rating::create($gameDuration, $id, $rank);
        $id = new RatingId('3');
        $rank = new Rank(3);
        $gameDuration = new Second(3779);
        $rating3 = Rating::create($gameDuration, $id, $rank);

        $ratingCollection = new RatingCollection();
        $ratingCollection->push($rating1);
        $ratingCollection->push($rating2);
        $ratingCollection->push($rating3);

        $mock = $this->getMockBuilder(RatingQueryRepository::class)->setMethods(['query', 'save'])->getMock();
        $mock->method('query')->with($this->ratingSpecification())->willReturn($ratingCollection);

        return $mock;
    }

    private function ratingSpecificationFactory()
    {
        $mock = $this->getMockBuilder(RatingSpecificationFactory::class)->setMethods(['getRatingSpecification'])->getMock();
        $mock->method('getRatingSpecification')->willReturn($this->ratingSpecification());

        return $mock;
    }

    private function ratingSpecification()
    {
        return $this->getMockBuilder(RatingSpecification::class)->getMock();
    }
}