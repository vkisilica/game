<?php

declare(strict_types=1);

namespace Game\Tests\Components\Rating\UseCases;

use Game\Components\Rating\Application\Persistence\RatingQueryRepository;
use Game\Components\Rating\Application\UseCases\CreateRating\Request;
use Game\Components\Rating\Application\UseCases\CreateRating\UseCase;
use Game\Components\Rating\Domain\Rating;
use Game\Components\Rating\Domain\Second;
use Game\Tests\Components\Rating\RatingTestCase;

class CreateRatingTest extends RatingTestCase
{
    /** @test */
    public function create_rating()
    {
        $request = Request::create(20);

        $response = $this->useCase()->handle($request);

        $this->assertNull($response);
    }

    private function useCase()
    {
        $seconds = new Second(20);
        $rating = Rating::create($seconds);

        $mock = $this->getMockBuilder(RatingQueryRepository::class)->getMock();
        $mock->method('save')->with($rating)->willReturn(null);

        return new UseCase($mock);
    }
}